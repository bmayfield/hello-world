FROM openjdk:8
COPY . hello-world
WORKDIR hello-world
RUN javac "HelloWorld.java"
CMD ["java", "HelloWorld"]
